function generateHashtag($str) {
  
  if (empty(trim($str))) {
    return false;
  }
  $spaces = explode(' ', $str);
  $result = '#';

  foreach ($spaces as $word) {
    $convert = ucfirst($word);
    $result .= $convert;
  }
  
  if (strlen($result) > 140) {
    return false;
  }
  
  
  return $result;
}
